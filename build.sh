docname="main"
tex="$docname.tex"

pdflatex $tex
pdflatex $tex
pdflatex $tex

cp "$docname.pdf" "git-gud.pdf"

apvlv "$docname.pdf" &

texcount -total -nosub $tex
